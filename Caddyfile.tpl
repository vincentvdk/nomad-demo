{{ env "NOMAD_META_TRAEFIK_HOST" }} {
    root local/caddyroot
    tls off
    log stdout
}
